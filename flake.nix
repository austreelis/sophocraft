{
  inputs = {
    nixpkgs.url = "nixpkgs";
    devshell = {
      url = "github:numtide/devshell";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    parts = {
      url = "github:hercules-ci/flake-parts";
      inputs.nixpkgs-lib.follows = "nixpkgs";
    };
  };

  outputs = inputs: let
    inherit (inputs) devshell nixpkgs parts;
    in
    parts.lib.mkFlake { inherit inputs; } {
      systems = [ "x86_64-linux" ];
      imports = [ devshell.flakeModule ];
      perSystem = { config, lib, pkgs, system, ... }:
        {
          _module.args.pkgs = import nixpkgs { inherit system; };
          devshells.default = {
            devshell = {
              name = "sophocraft";
              meta = {
                description = "Quilt mod for Minecraft for very vanilla-style and survival-friendly progression & automation";
              };
              packages = [
                pkgs.alsa-lib
                pkgs.flite
                pkgs.libpulseaudio
                pkgs.openal
                pkgs.jdt-language-server
              ];
            };
            commands = [
              { category = "nix"; package = pkgs.nixpkgs-fmt; }
              { category = "java"; package = pkgs.gradle; }
              { category = "java"; package = pkgs.jdk17; }
            ];
            env = [
              # {
                # name = "GRADLE_USER_HOME";
                # eval = ''"$PRJ_DATA_DIR"/gradle'';
              # }
              # {
                # name = "ORG_GRADLE_PROJECT_buildDir";
                # eval = "\"$PRJ_DATA_DIR\"/build";
              # }
              {
                name = "LD_LIBRARY_PATH";
                eval =
                  "$DEVSHELL_DIR/lib\${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}";
              }
            ];
          };
        };
    };
}
