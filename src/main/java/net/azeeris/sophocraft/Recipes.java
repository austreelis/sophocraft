package net.azeeris.sophocraft;

import org.quiltmc.qsl.recipe.api.RecipeLoadingEvents.RemoveRecipesCallback;

import net.minecraft.item.ItemStack;
import net.minecraft.recipe.Recipe;
import net.minecraft.registry.DynamicRegistryManager;
import net.minecraft.util.Identifier;

class Recipes {
    static void removeRedundant(RemoveRecipesCallback.RecipeHandler handler) {
        handler.removeIf(recipe -> {
            if (Recipes.isRedundant(recipe, handler.getRegistryManager())) {
                Mod.LOGGER.info("removing recipe {}", recipe.getId());
                return true;
            }
            return false;
        });
    }

    static boolean isRedundant(Recipe<?> recipe, DynamicRegistryManager registryManager) {
        Identifier id = recipe.getId();
        if (id.getNamespace() != "minecraft") {
            return false;
        }
        ItemStack result = recipe.getResult(registryManager);
        return result.isOf(Items.FURNACE)
            || result.isOf(Items.BLAST_FURNACE)
            || result.isOf(Items.COPPER_INGOT)
            || result.isOf(Items.IRON_INGOT)
            || result.isOf(Items.GOLD_INGOT);
    }
}
