package net.azeeris.sophocraft;

import org.quiltmc.qsl.item.setting.api.QuiltItemSettings;

import net.fabricmc.fabric.api.itemgroup.v1.ItemGroupEvents;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroups;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.util.Identifier;

class Items extends net.minecraft.item.Items {
    static final Item BEDROCK_HAMMER = register(
        "bedrock_hammer",
        new Item(new QuiltItemSettings().recipeSelfRemainder())
    );
    static final Item COPPER_NUGGET = register("copper_nugget");
    static final Item CRUSHED_FLINT = register("crushed_flint");
    static final Item FIRE_CLAY_BALL = register("fire_clay_ball");
    static final Item FIRE_BRICK = register("fire_brick");
    static final Item FLINT_CLAY_BALL = register("flint_clay_ball");
    static final Item HIGH_GRADE_FIRE_BRICK = register("high_grade_fire_brick");
    static final Item TUFF_POWDER = register("tuff_powder");

    static void registerItemGroupEventsHandlers() {
        Mod.LOGGER.info("Registering item group events handlers");
        ItemGroupEvents.modifyEntriesEvent(ItemGroups.INGREDIENTS).register(content -> {
            content.addBefore(IRON_NUGGET, COPPER_NUGGET);
            content.addAfter(CLAY_BALL, FIRE_CLAY_BALL);
            content.addAfter(FIRE_CLAY_BALL, FLINT_CLAY_BALL);
            content.addAfter(BRICK, FIRE_BRICK);
            content.addAfter(FIRE_BRICK, HIGH_GRADE_FIRE_BRICK);
            content.addAfter(FLINT, CRUSHED_FLINT);
            content.addAfter(CRUSHED_FLINT, TUFF_POWDER);
        });
        ItemGroupEvents.modifyEntriesEvent(ItemGroups.TOOLS_AND_UTILITIES).register(content -> {
            content.addAfter(net.minecraft.item.Items.NETHERITE_HOE, BEDROCK_HAMMER);
        });
    }

    static Item register(String id) {
        return register(id, new Item(new QuiltItemSettings()));
    }

    static Item register(String id, Item item) {
        return register(new Identifier(Mod.ID, id), item);
    }

    static Item register(Identifier id) {
        return register(id, new Item(new QuiltItemSettings()));
    }

    static Item register(Identifier id, Item item) {
        if (item instanceof BlockItem) {
            ((BlockItem)item).appendBlocks(Item.BLOCK_ITEMS, item);
        }

        return Registry.register(Registries.ITEM, id, item);
    }
}