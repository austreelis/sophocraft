package net.azeeris.sophocraft;

import org.quiltmc.loader.api.ModContainer;
import org.quiltmc.qsl.base.api.entrypoint.ModInitializer;
import org.quiltmc.qsl.recipe.api.RecipeManagerHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Mod implements ModInitializer {
    public static final String ID = "sophocraft";

    static final Logger LOGGER = LoggerFactory.getLogger("Sophocraft");


    @Override
    public void onInitialize(ModContainer mod) {
        LOGGER.info("Initializing {}", mod.metadata().name());
        Items.registerItemGroupEventsHandlers();
        RecipeManagerHelper.removeRecipes(Recipes::removeRedundant);
    }
}
